;;;; mandel.lisp

(in-package #:mandel)

(defun clamp (x a b)
  (max a (min x b)))

(defun scale (x min-x max-x a b)
  (+ (* (- b a) (/ (- x min-x) (- max-x min-x))) a))

(defmacro let/ret ((var-sym value-form &optional (result-form nil result-form-supplied-p)) &body forms)
  `(let ((,var-sym ,value-form))
     (prog1 ,(or (and result-form-supplied-p result-form) var-sym)
       ,@forms)))

(defstruct rgb-color
  (red 0 :type (unsigned-byte 8))
  (green 0 :type (unsigned-byte 8))
  (blue 0 :type (unsigned-byte 8)))

(defstruct (ppm (:constructor make-uninitialised-ppm))
  (width 0 :type fixnum)
  (height 0 :type fixnum)
  (data nil))

(defun make-ppm (width height)
  (let ((data (make-array (list width height)
                          :element-type 'rgb-color
                          :initial-element (make-rgb-color :red 0 :green 0 :blue 0))))
    (make-uninitialised-ppm :width width :height height :data data)))

(defun ppm-get (ppm x y)
  (aref (ppm-data ppm) x y))

(defun ppm-set (ppm x y c)
  (setf (aref (ppm-data ppm) x y) c))

(defun ppm-to-string (ppm)
  (let ((ppm-string (make-array 0 :element-type 'base-char :fill-pointer 0 :adjustable t)))
    (prog1 ppm-string
      (with-output-to-string (s ppm-string)
        (with-slots (width height data) ppm
          (format s "P3~%~a ~a 255~%" width height)
          (dotimes (x width)
            (dotimes (y height)
              (with-slots (red green blue) (ppm-get ppm x y)
                (format s "~a ~a ~a " red green blue)))))))))

(defun ppm-write (ppm path)
  (let ((ppm-string (ppm-to-string ppm)))
    (with-open-file (f path :direction :output :if-exists :supersede)
      (write-string ppm-string f))))

(defun pixel-color (escape-value max-iter)
  (if (> escape-value max-iter)
    (make-rgb-color :red 0 :green 0 :blue 0)
    (let ((c (* (/ (log escape-value) (log (- max-iter 1))) 3)))
      (cond
        ((< c 1) (make-rgb-color :red (clamp (floor (* c 255)) 0 255)
                                 :green 0
                                 :blue 0))
        ((< c 2) (make-rgb-color :red 255
                                 :green (clamp (floor (* (- c 1) 255)) 0 255)
                                 :blue 0))
        (t (make-rgb-color :red 255
                           :green 255
                           :blue (clamp (floor (* (- c 2) 255)) 0 255)))))))

(defun mandelbrot (x y width height max-iter)
  (do* ((c (complex (scale x 0 width -1.5 0.5) (scale y 0 height -1 1)))
        (z (complex 0 0) (+ (expt z 2) c))
        (cnt 0 (1+ cnt)))
    ((or (> cnt max-iter) (> (abs z) 2)) cnt)))

(defun mandel (width height &optional (max-iter 1000))
  (let/ret (image (make-ppm width height))
    (dotimes (y height)
      (dotimes (x width)
        (let ((escape-value (mandelbrot x y width height max-iter)))
          (ppm-set image x y (pixel-color escape-value max-iter)))))))
